// CONSTRUCTOR FOR PERSON

        function person(name, lastname, img, sex, age, birth, address, mail, phone, notes, contact) {
            this.name = name;
            this.lastname = lastname;
            this.img = img;
            this.sex = sex;
            this.age = age;
            this.birth = birth;
            this.address = address;
            this.mail = mail;
            this.phone = phone;
            this.notes = notes;
            this.contact = contact;
            this.print = function(){
               
                     return `<li class="contact-li">
                                    <img src="${this.img}">
                                    <div class="con-name-holder">
                                      <span class="li-name">${this.name}</span><br>
                                      <span class="bold li-lastname"> ${this.lastname}</span>
                                    </div>
                                    <i class="fas fa-chevron-right"></i>
                             </li>`;
            };
        }

        var jane = new person("Jane", "Doe", "images/person1.jpg", "Female", "26", "November 10, 1992", "123 Fake Street Number 1", "mailone@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person1.jpg");
        var john = new person("John", "Cole", "images/person2.jpg", "Male", "27", "March 09, 1991", "123 Fake Street Number 2", "mailtwo@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person7.jpg");
        var david = new person("David", "Gonzalez", "images/person3.jpg", "Male", "36", "July 13, 1982", "123 Fake Street Number 3", "mailthree@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person6.jpg");
        var eric = new person("Eric", "Koston", "images/person4.jpg", "Male", "34", "July 01, 1984", "123 Fake Street Number 4", "mailfour@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person4.jpg");
        var eva = new person("Eva", "Green", "images/person5.jpeg", "Female", "30", "June 16, 1988", "123 Fake Street Number 5", "mailfive@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person3.jpg");
        var sean = new person("Sean", "Malto", "images/person6.jpg", "Male", "28", "August 01, 1990", "123 Fake Street Number 6", "mailsix@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person2.jpg");
        var sandra = new person("Sandra", "Black", "images/person7.jpg", "Female", "31", "July 01, 1987", "123 Fake Street Number 7", "mailseven@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person1.jpg");
        var paul = new person("Paul", "Rodriguez", "images/person8.jpg", "Male", "27", "May 05, 1991", "123 Fake Street Number 8", "maileight@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person9.jpg");
        var bill = new person("Bill", "Jackson", "images/person9.jpg", "Male", "58", "January 28, 1960", "123 Fake Street Number 9", "mailnine@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person8.jpg");
        var trevor = new person("Trevor", "Brown", "images/person10.jpg", "Male", "25", "October 07, 1993", "123 Fake Street Number 10", "mailten@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person7.jpg");
        var bob = new person("Bob", "Collins", "images/person11.jpeg", "Male", "34", "February 24, 1984", "123 Fake Street Number 11", "maileleven@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person6.jpg");
        var chaz = new person("Chaz", "Ortiz", "images/person12.jpg", "Male", "28", "May 03, 1990", "123 Fake Street Number 12", "mailtwelve@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person1.jpg");
        var tom = new person("Tom", "Harris", "images/person13.jpg", "Male", "35", "June 01, 1983", "123 Fake Street Number 13", "mailthirt@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person4.jpg");
        var lucy = new person("Lucy", "Chan", "images/person14.jpeg", "Female", "26", "January 09, 1992", "123 Fake Street Number 14", "mailfourt@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person3.jpg");
        var jerry = new person("Jerry", "Johnes", "images/person15.jpeg", "Male", "34", "July 02, 1984", "123 Fake Street Number 15", "mailfift@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person2.jpg");
        var kate = new person("Kate", "Ryan", "images/person16.jpg", "Female", "27", "May 17, 1991", "123 Fake Street Number 16", "mailsixt@mail.com", "123-456-7890", 
                              "Lorem ipsum dolor sit amet, consectetur adipisicing elit. A, magnam iure sapiente sit quos deserunt quae ipsam. Asperiores ad tenetur odit, at sapiente enim qui amet voluptatum! Officiis, provident, a!", 
                              "images/person1.jpg");
        

        
        var persons = [jane, john, david, eric, eva, sean, sandra, paul, bill, trevor, bob, chaz, tom, lucy, jerry, kate];



// LOOP AND PRINT THE CONTACTS
    for ( var i = 0; i < persons.length; i++) {
        document.getElementById("contacts-list").innerHTML += persons[i].print();
    } 


// CLICK ON CONTACTS
$(function(){
    $(document).on("click", ".contact-li", function(){ 

            let name = $(this).find(".li-name").text().trim();
           
            let lastname = $(this).find(".li-lastname").text().trim();
           
            let person = persons.filter((val) => {
                return (val.name === name && val.lastname === lastname);
            });

            $("#contact-name").text(person[0].name + ' ' + person[0].lastname);
            $("#contact-img").css('background-image', 'url('+ person[0].img +')');
            $("#contact-sex").text(person[0].sex);
            $("#contact-age").text(person[0].age);
            $("#contact-birth").text(person[0].birth);
            $("#contact-address").text(person[0].address);
            $("#contact-phone").text(person[0].phone);
            $("#contact-phone2").text(person[0].phone);
            $("#contact-email").text(person[0].mail);
            $("#contact-email2").text(person[0].mail);
            $("#contact-info").text(person[0].notes);
            $("#contact-con").attr("src" , person[0].contact);
     });  
});


// SEARCH FUNCTION

function searchContact() {
    // Declare variables
    var input, filter, ul, li, liname, i;
    input = document.getElementById("search");
    filter = input.value.toUpperCase();
    ul = document.getElementById("contacts-list");
    li = ul.getElementsByTagName("li");

    // Loop through all list items, and hide those who don't match the search query
    for (i = 0; i < li.length; i++) {
        liname = li[i].getElementsByClassName("li-name")[0];
        if (liname.innerHTML.toUpperCase().indexOf(filter) === 0) {
            li[i].style.display = "";
            flag = 1;
        } else {
            li[i].style.display = "none";
        } 
    }
}

// MARK LESSON

$(document).on("click", ".btn-attended", function(){
    var border = $(this).parents(".lesson-info").siblings(".marked");
    var statusWrapper = $(this).parents(".lesson-status-wrapper");
    border.removeClass("lesson-border");
    border.addClass("lesson-border1"); 
    statusWrapper.hide();  
});
$(document).on("click", ".btn-missed", function(){
    var border = $(this).parents(".lesson-info").siblings(".marked");
    var statusWrapper = $(this).parents(".lesson-status-wrapper");
    border.removeClass("lesson-border");
    border.addClass("lesson-border2"); 
    statusWrapper.hide();  
});

 


 // CONSTRUCTOR FOR LESSON

        function lesson(day, time, weekly, service, location, status) {
            this.day = day;
            this.time = time;
            this.weekly = weekly;
            this.service = service;
            this.location = location;
            this.status = status;
            this.print = function(){
               
                     return `<div class="lesson">
                                <div class="marked lesson-border"></div>
                                <div class="lesson-day">
                                    <span class="bold class-day">${this.day}</span><span class="bold class-time">${this.time}</span>
                                    <img src="images/hourglass.png" class="hourglass">
                                    <span class="bold class-week">${this.weekly}</span>
                                    <div class="btn-group pull-right lesson-btn">
                                        <button type="button" class="btn-action btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Choose an action
                                            <span class="caret"></span>
                                        </button>
                                        <ul class="dropdown-menu">
                                            <li>
                                                <a href="#">Action</a>
                                            </li>
                                            <li>
                                                <a href="#">Another action</a>
                                            </li>
                                            <li>
                                                <a href="#">Something else here</a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="lesson-info">
                                    <div class="row">
                                        <div class="col-md-2 col-sm-4 col-xs-12 cut-text">
                                            <h5 class="grey">Service</h5>
                                            <h4 class="bold dimgrey">${this.service}</h4>
                                        </div>
                                        <div class="col-md-2 col-sm-4 col-xs-12 cut-text">
                                            <h5 class="grey">Location</h5>
                                            <h4 class="bold dimgrey">${this.location}</h4>
                                        </div>
                                        <div class="col-md-2 col-sm-4 col-xs-12 cut-text">
                                            <h5 class="grey">Status</h5>
                                            <h4 class="bold dimgrey">${this.status}</h4>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12 col-sm-12 bg-danger text-danger lesson-status-wrapper">
                                            <div class="lesson-status">
                                                <span>Lesson still needs to be marked</span>

                                                <button class="btn btn-sm btn-danger btn-missed pull-right">Missed</button>

                                                <button class="btn btn-sm btn-success btn-attended pull-right">Attended</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                             </div`;
            };
        }


        var lesson1 = new lesson("Tuesday", "9:00am", "60 / Weekly", "Switch Flip 101", "Fantasy Fair", "Paid");
        var lesson2 = new lesson("Thursday", "2:00pm", "60 / Bi-Weekly", "Mega Ramp Practice", "The Berricks", "Canceled");
        var lesson3 = new lesson("Saturday", "12:45pm", "90 / Bi-Weekly", "S.K.A.T.E. Session.", "The Berricks", "Paid");
        var lesson4 = new lesson("Tuesday", "9:00am", "60 / Weekly", "Switch Flip 101", "Fantasy Fair", "Paid");
        var lesson5 = new lesson("Thursday", "2:00pm", "60 / Bi-Weekly", "Mega Ramp Practice", "The Berricks", "Canceled");
        var lesson6 = new lesson("Saturday", "12:45pm", "90 / Bi-Weekly", "S.K.A.T.E. Session.", "The Berricks", "Paid");


        var lessons = [lesson1, lesson2, lesson3, lesson4, lesson5, lesson6];

        // LOOP AND PRINT THE LESSONS
        for ( var i = 0; i < lessons.length; i++) {
        document.getElementById("lessons").innerHTML += lessons[i].print();
    } 
